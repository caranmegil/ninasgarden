/*
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { useEffect, useState } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import './styles.css';
import getNinasGarden from '../../queries/getNinasGarden';

function SensorData() {
    const [sensorData, setSensorData] = useState({
        getSensorData: {
            ninasGarden: {
                moisture: 0,
                temperature: 0.00,
            },
        }
    });

    const client = useApolloClient();

    useEffect(() => {
        async function queryAPI() {
            try {
                let result = await client.query({
                    query: getNinasGarden,
                    fetchPolicy: 'no-cache',
                });
                console.log(getNinasGarden)
                setSensorData(result.data);
            } catch(e: any) {
                console.error(e);
            }
        }

        const intervalId = setInterval( async () => {
            await queryAPI();
        }, 1000);

        return () => clearInterval(intervalId);        
    }, [client]);

    return (
        <div>
            <p>
                <h2>Moisture</h2>
                <span>{sensorData.getSensorData.ninasGarden.moisture}</span>
            </p>
            <p>
                <h2>Temperature</h2>
                <span>{Math.round((sensorData.getSensorData.ninasGarden.temperature * 9 / 5) + 32)} F / {Math.round(sensorData.getSensorData.ninasGarden.temperature)} C</span>
            </p>
        </div>
    );
}

export default SensorData;