import { request, gql } from 'graphql-request';
import axios, { AxiosResponse } from 'axios';

const saveNinasGardenMutation = gql`
	mutation Mutation($input: NinasGardenInput) {
		saveNinasGarden(input: $input) {
			moisture
			temperature
		}
	}
`;

setInterval(async () => {
	try {
		const response: AxiosResponse = await axios.get('http://127.0.0.1:5000');
		await request('https://caranmegil.com/graphql', saveNinasGardenMutation, {
	 		input: response.data,
		});
	} catch(err) {
		console.error(err);
	}
}, 1000);

