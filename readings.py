# readings.py - Driver of querying service
# Copyright (C) 2022  William R. Moore <william@nerderium.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import board

from flask import Flask

app = Flask(__name__)

from adafruit_seesaw.seesaw import Seesaw

i2c_bus = board.I2C()

ss = Seesaw(i2c_bus, addr=0x36)

@app.route('/')
def get_readings():
    moisture = ss.moisture_read()
    temp = ss.get_temp()
    return {"moisture": moisture, "temperature": temp}

